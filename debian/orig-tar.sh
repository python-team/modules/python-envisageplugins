#!/bin/sh -e

# called by uscan with '--upstream-version' <version> <file>
DIR=python-envisageplugins-$2.orig

# cleanup the upstream sources
tar zxf $3
mv EnvisagePlugins-$2 $DIR

# remove freetype2 source from enthought.kiva
(cd $DIR; find -iname splash.jpg -delete)

# create the tarball
GZIP=--best tar -c -z -f $3 $DIR
rm -rf $DIR

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $3 $origDir
  echo "moved $3 to $origDir"
fi

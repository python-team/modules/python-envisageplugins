INFO = {
    'name': 'EnvisagePlugins',
    'version': '3.2.0',
    'install_requires' : [
        'EnvisageCore >= 3.2.0.dev',
    ],
}
